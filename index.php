<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="css/core-v1.2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

    <title>Hello, world!</title>
  </head>
  <body>
    <div id="headku" class="container-fluid bg-info" style="padding-top:70px;padding-bottom:70px">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="logo-circle">
                        <div class="text">Picfubri.</div>
                    </div>
                    <h1>
                        We Create.<br>
                        We Innovate.<br>
                        We Share.
                    </h1>
                </div>
                <div class="col">
                    <div class="bts">
                        <div class="pic-fonts">
                        Get the latest information from us.
                        </div>
                        <button type="button" class="pic-btn" data-toggle="modal" data-target="#register">Register</button>
                        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Register Form</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Woohoo, you're reading this text in a modal!
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="pic-btn" data-toggle="modal" data-target="#login">Login</button>
                        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Login Form</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Woohoo, you're reading this text in a modal!
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <i class="fa fa-github" style="font-size:36px; color:white;"></i>
                        <i class="fa fa-facebook-square" style="font-size:36px; color:white;"></i>
                        <i class="fa fa-twitter-square" style="font-size:36px; color: white;"></i>
                        <i class="fa fa-instagram" style="font-size:36px; color: white;"></i>
                        <i class="fa fa-youtube-play" style="font-size:36px; color: white;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="bodyku" class="container-fluid bg-light" style="padding-top:70px;padding-bottom:0px">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow-sm p-3 mb-5 bg-white rounded" style="border-style:solid; border-color:grey; text-align:center;">
                        <h5>Our Product</h5>
                        <hr>
                        <a href="#" class="badge badge-primary">Ardu Project</a>
                        <a href="#" class="badge badge-primary">Academy</a>
                        <a href="#" class="badge badge-primary">E-Book Store</a>
                        <a href="#" class="badge badge-primary">News</a>
                        <a href="#" class="badge badge-primary">PicAnim</a>
                        <a href="#" class="badge badge-primary">Qur'an Online</a>
                    </div>
                </div>
                <div class="col">
                    <div class="shadow-sm p-3 mb-5 bg-white rounded">
                        <h5>We Create.</h5>
                        <hr>
                        <p>By creating something new, we want to make changes that are beneficial to our environment.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="shadow-sm p-3 mb-5 bg-white rounded">
                        <h5>We Innovate.</h5>
                        <hr>
                        <p>Innovation is our goal, because with innovation we can contribute to our environment.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="shadow-sm p-3 mb-5 bg-white rounded">
                        <h5>We Share.</h5>
                        <hr>
                        <p>Sharing is an absolute right for us, because by sharing we can advance a nation and country.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="body-section" class="container-fluid bg-white" style="padding-top:70px;padding-bottom:25px">
        <div class="container">
        <h3>Product</h3>
        <hr>
            <div class="row">
                <div class="col-sm">
                    <div id="game">
                        <img src="Asset/svg/gamepad.svg" class="img-fluid" alt="Responsive image" width="100px">
                        <h4>Games</h4>
                    </div>
                </div>
                <div class="col-sm">
                    <div id="android">
                        <img src="Asset/svg/android.svg" class="img-fluid" alt="Responsive image" width="100px">
                        <h4>Android</h4>
                    </div>
                </div>
                <div class="col-sm">
                    <div id="web">
                        <img src="Asset/svg/web.svg" class="img-fluid" alt="Responsive image" width="100px">
                        <h4>WEB</h4>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div id="body-section" class="container-fluid bg-info" style="padding-top:70px;padding-bottom:25px">
        <div class="container">
        <h3>Portfolio</h3>
        <hr>
            <div class="row">
                <div class="col">
                    <div class="col-text">
                        <img src="Asset/img/pf_img1.png">
                    </div>    
                </div>
                <div class="col">
                    <div class="col-text">
                    <img src="Asset/img/pf_img2.png">
                    </div>    
                </div>
                <div class="col">
                    <div class="col-text">
                    <img src="Asset/img/pf_img3.png">
                    </div>    
                </div>
                <div class="col">
                    <div class="col-text">
                        col
                    </div>    
                </div>
            </div>
        </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>